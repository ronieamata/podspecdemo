Pod::Spec.new do |spec|
  spec.name = "podspecsdemo"
  spec.version = "1.0.3"
  spec.summary = "Sample framework from blog post, not for real world use.HHHHH"
  spec.homepage = "https://github.com/jakecraige/RGB"
  spec.license = { type: 'MIT', file: 'LICENSE' }
  spec.authors = { "Your Name" => 'your-email@example.com' }
  spec.social_media_url = "http://twitter.com/sample"

  spec.platform = :ios, "11.2"
  spec.requires_arc = true
  spec.source = { git: "https://gitlab.com/ronieamata/podspecdemo.git", tag: "v#{spec.version}", submodules: true }
  spec.source_files = "podspecdemo/**/*.{h,swift}"
end